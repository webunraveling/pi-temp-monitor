These are some slapped together Python scripts for monitoring temperaturesusing DHT sensors on the GPIO of Raspberry Pi.

# Installation
Make sure you have done `apt install python3-pip python-requests python-time` and then `sudo pip3 install Adafruit_DHT`.

# Resources
Look up the GPIO pin diagram to find out which pins you can hook up to. Pay attention to whether or not the pin with power has 3V or 5V. 3V should be fine but just pay attention to that.

* [Weather.gov API docs](https://www.weather.gov/documentation/services-web-api)
* [A guide for setting up](https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/)

import os
import time
import requests
import Adafruit_DHT

# This script will run once. It writes values to a CSV and exits. You can set this up
# on a cron. Alternatively you could create an infinite loop (e.g. while true) with a
# sleep time at the end.

# Does an API call and returns the current temperature.
def query_api():

    api_url = 'https://api.weather.gov/stations/KBJI/observations/latest'

    response = requests.get(api_url)

    # The API response as JSON.
    response_dict = response.json()

    if response.status_code < 400 and response_dict['properties']['temperature']['value'] is not None:
        temperature_output = response_dict['properties']['temperature']['value'] * 9/5.0 + 32
    else:
        temperature_output = 'fail'
        print(f"API message: HTTP status: {response.status_code}; Value: {response_dict['properties']['temperature']['value']}")

    return temperature_output

DHT_SENSOR = Adafruit_DHT.DHT22
DHT_PIN = 4
file_location = '/home/temp/temperatures.csv'
physical_location = 'inside'
temperature_outside = query_api()

try:
    f = open(file_location, 'a+')
    if os.stat(file_location).st_size == 0:
            f.write('Date,Time,Outside Temp F,Inside Temp F,Humidity\r\n')
except:
    pass

humidity, temperature = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)

if temperature is not None:
    temperature = temperature * 9/5.0 + 32
    f.write('{0},{1},{2:0.1f},{3:0.1f},{4:0.1f}%\r\n'.format(time.strftime('%m/%d/%y'), time.strftime('%H:%M'), temperature_outside, temperature, humidity))
else:
    print("Failed to retrieve data from humidity sensor. Check wiring.")

# Complete Project Details: https://RandomNerdTutorials.com/raspberry-pi-dht11-dht22-python/

import Adafruit_DHT
import time
import requests

# Does an API call and returns the current temperature.
def query_api():

    api_url = 'https://api.weather.gov/stations/KBJI/observations/latest'

    response = requests.get(api_url)

    # The API response as JSON.
    response_dict = response.json()

    if response.status_code < 400 and response_dict['properties']['temperature']['value'] is not None:
        temperature_output = response_dict['properties']['temperature']['value'] * 9/5.0 + 32
    else:
        temperature_output = 'fail'
        print(f"API message: HTTP status: {response.status_code}; Value: {response_dict['properties']['temperature']['value']}")

    return temperature_output

temperature_outside = query_api()

# comment and uncomment the lines below depending on your sensor
sensor = Adafruit_DHT.DHT22
# sensor = Adafruit_DHT.DHT11

# DHT pin connects to GPIO 4
sensor_pin = 4

while True:
    humidity, temperature = Adafruit_DHT.read_retry(sensor, sensor_pin)  # Use read_retry to retry in case of failure
    if humidity is not None and temperature is not None:
        # Convert to F
        temperatureF = temperature * 9/5.0 + 32
        print("Outside={0:0.1f}ºF, Inside={1:0.1f}ºF, Humidity={2:0.1f}%".format(temperature_outside, temperatureF, humidity))
    else:
        print("Sensor failure. Check wiring.")

    time.sleep(5)

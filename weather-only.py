import requests
import time

# Does an API call and returns the current temperature.
def query_api():

    api_url = 'https://api.weather.gov/stations/KBJI/observations/latest'

    response = requests.get(api_url)

    # The API response as JSON.
    response_dict = response.json()

    if response.status_code < 400 and response_dict['properties']['temperature']['value'] is not None:
        temperature = response_dict['properties']['temperature']['value'] * 9/5.0 + 32
        temperature_output = temperature
    else:
        temperature_output = f"(HTTP status: " + response.status_code + "; Value: " + response_dict['properties']['temperature']['value'] + ")"


